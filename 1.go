package main

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	_ "regexp"
	"strconv"
	"strings"

	"github.com/dveselov/mystem"
)

// Обработка ошибок
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

// Пара функций для округления результатов
func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

// Удаление сносок [xxx], если они есть и удаление всех символов
// Так же удаление сносок вида [источник не указан 589 дней]
func removeFootnote(text string) string {
	var start, finish, delta int

	for strings.Contains(text, "[") == true {
		start = strings.Index(text, "[")
		finish = strings.Index(text, "]")

		if start == -1 || finish == -1 {
			return text
		}

		delta = finish - start

		// Подозреваем что это сноска
		if delta >= 1 && delta <= 3 {
			//fmt.Println(text[start : finish+1])
			text = text[:start] + text[finish+1:]
		} else {
			start_long_0 := strings.Index(text, "[источник не указан")
			start_long_1 := strings.Index(text, "[уточните ссылку")
			start_long_2 := strings.Index(text, "[страница не указана")

			if start == start_long_0 || start == start_long_1 || start == start_long_2 {
				//fmt.Println(text[start : finish+1])
				text = text[:start] + text[finish+1:]
			} else {
				text = strings.Replace(text, "[", " ", 1)
				text = strings.Replace(text, "]", " ", 1)
			}
		}
	}

	return text
}

// Удаление знаков препинания
func remove_symbols(text string) string {
	text = strings.Replace(text, " г. ", " год ", -1)
	text = strings.Replace(text, "здача", "сдача", -1)
	text = strings.Replace(text, "Крга", "когда", -1)
	text = strings.Replace(text, "<", " меньше ", -1)
	text = strings.Replace(text, ">", " больше ", -1)
	text = strings.Replace(text, "’", "", -1)
	text = strings.Replace(text, "1ntel", "intel", -1)
	text = strings.Replace(text, "“", " ", -1)
	text = strings.Replace(text, "„", " ", -1)
	text = strings.Replace(text, "1nitiative", "initiative", -1)
	text = strings.Replace(text, "⇧", " ", -1)
	text = strings.Replace(text, "1so", "iso", -1)
	text = strings.Replace(text, "1ml", "1 ml", -1)
	text = strings.Replace(text, "°", " градус ", -1)
	text = strings.Replace(text, "′", " ", -1)
	text = strings.Replace(text, "женнсие", "женские", -1)
	text = strings.Replace(text, "предстиавлены", "представлены", -1)
	text = strings.Replace(text, "ктосчитает", "кто считает", -1)
	text = strings.Replace(text, "тыс.", "тысяч ", -1)
	text = strings.Replace(text, "(i)", "", -1)
	text = strings.Replace(text, "облигакции", "облигации", -1)
	text = strings.Replace(text, "анлиз", "анализ", -1)
	text = strings.Replace(text, "л. с.", " лошадиных сил ", -1)
	text = strings.Replace(text, "ЦБ", "центральный банк", -1)
	text = strings.Replace(text, "РФ", "Российская федерация россия", -1)
	text = strings.Replace(text, " рф ", " Российская федерация россия ", -1)
	text = strings.Replace(text, "…", " ", -1)
	text = strings.Replace(text, "ст.-сл.", "старо славянский", -1)
	text = strings.Replace(text, "ц.-сл.", "церковно славянский", -1)
	text = strings.Replace(text, "€", " евро ", -1)
	text = strings.Replace(text, "к. н.", "кандидат наук", -1)
	text = strings.Replace(text, "н. э.", "нашей эры", -1)
	text = strings.Replace(text, "нем.", "", -1)
	text = strings.Replace(text, "араб.", "", -1)
	text = strings.Replace(text, "см.", "", -1)
	text = strings.Replace(text, "фр.", "", -1)
	text = strings.Replace(text, "др.-греч.", "", -1)
	text = strings.Replace(text, "греч.", "", -1)
	text = strings.Replace(text, "лат.", "", -1)
	text = strings.Replace(text, "англ.", "", -1)
	text = strings.Replace(text, "\u00ad", "", -1)
	text = strings.Replace(text, "рус.", "", -1)
	text = strings.Replace(text, "итал.", "", -1)
	text = strings.Replace(text, "франц.", "", -1)
	text = strings.Replace(text, "см.", "", -1)
	text = strings.Replace(text, "²", " площадь ", -1)
	text = strings.Replace(text, "р.", " рубли ", -1)
	text = strings.Replace(text, "руб ", " рубли ", -1)
	text = strings.Replace(text, "млрд", " миллиард ", -1)
	text = strings.Replace(text, "’s", "", -1)
	text = strings.Replace(text, "ирл.", "", -1)
	text = strings.Replace(text, "местн.", "", -1)
	text = strings.Replace(text, "т.", " тонн ", -1)
	text = strings.Replace(text, "·", "", -1)
	text = strings.Replace(text, "м/с", " метров в секунду скорость ", -1)
	text = strings.Replace(text, "ст.", " статья ", -1)
	text = strings.Replace(text, "pH", " кислотность ", -1)
	text = strings.Replace(text, "°C", " градус ", -1)
	text = strings.Replace(text, "%", " процент ", -1)
	text = strings.Replace(text, "е́", "е", -1)
	text = strings.Replace(text, "é", "e", -1)
	text = strings.Replace(text, "а́", "а", -1)
	text = strings.Replace(text, "о́", "о", -1)
	text = strings.Replace(text, "и́", "и", -1)
	text = strings.Replace(text, ".", " ", -1)
	text = strings.Replace(text, ",", " ", -1)
	text = strings.Replace(text, "!", " ", -1)
	text = strings.Replace(text, "_", " ", -1)
	text = strings.Replace(text, "~", " ", -1)
	text = strings.Replace(text, "?", " ", -1)
	text = strings.Replace(text, "(", " ", -1)
	text = strings.Replace(text, ")", " ", -1)
	text = strings.Replace(text, "/", " ", -1)
	text = strings.Replace(text, "\\", "", -1)
	text = strings.Replace(text, "-м", "", -1)
	text = strings.Replace(text, "-е", "", -1)
	text = strings.Replace(text, "-х", "", -1)
	text = strings.Replace(text, "-x", "", -1)
	text = strings.Replace(text, "[", " ", -1)
	text = strings.Replace(text, "]", " ", -1)
	text = strings.Replace(text, ":", " ", -1)
	text = strings.Replace(text, "+", " ", -1)
	text = strings.Replace(text, "*", " ", -1)
	text = strings.Replace(text, "&", " ", -1)
	text = strings.Replace(text, "^", " ", -1)
	text = strings.Replace(text, ";", " ", -1)
	text = strings.Replace(text, "'", " ", -1)
	text = strings.Replace(text, "\"", " ", -1)
	text = strings.Replace(text, "-", " ", -1)
	text = strings.Replace(text, "—", " ", -1)
	text = strings.Replace(text, "=", " равно ", -1)
	text = strings.Replace(text, "№", " номер ", -1)
	text = strings.Replace(text, "#", " номер ", -1)
	text = strings.Replace(text, "СПб", "Санкт Петербург", -1)
	text = strings.Replace(text, "МСК", "Москва", -1)
	text = strings.Replace(text, "ДВС", "двигатель внутренного сгорания", -1)
	text = strings.Replace(text, "гг", " годах ", -1)
	text = strings.Replace(text, "ФЗ", " федеральный закон ", -1)
	text = strings.Replace(text, "УГ", " уголовный кодекс ", -1)
	text = strings.Replace(text, "млн", "миллион", -1)
	text = strings.Replace(text, "США", "Соединенные Штаты Америки", -1)
	text = strings.Replace(text, "€", " евро ", -1)
	text = strings.Replace(text, "$", " доллар ", -1)
	text = strings.Replace(text, "Ё", "Е", -1)
	text = strings.Replace(text, "ё", "е", -1)
	text = strings.Replace(text, " XXV ", "25", -1)
	text = strings.Replace(text, " XXIV ", "24", -1)
	text = strings.Replace(text, " XXIII ", "23", -1)
	text = strings.Replace(text, " XXII ", "22", -1)
	text = strings.Replace(text, " XXI ", "21", -1)
	text = strings.Replace(text, " XX ", "20", -1)
	text = strings.Replace(text, " XIX ", "19", -1)
	text = strings.Replace(text, " XVIII ", "18", -1)
	text = strings.Replace(text, " XVII ", "17", -1)
	text = strings.Replace(text, " XVI ", "16", -1)
	text = strings.Replace(text, " XV ", "15", -1)
	text = strings.Replace(text, " XIV ", "14", -1)
	text = strings.Replace(text, " XIII ", "13", -1)
	text = strings.Replace(text, " XII ", "12", -1)
	text = strings.Replace(text, " XI ", "11", -1)
	text = strings.Replace(text, " X ", "10", -1)
	text = strings.Replace(text, " IX ", "9", -1)
	text = strings.Replace(text, " VIII ", "8", -1)
	text = strings.Replace(text, " VII ", "7", -1)
	text = strings.Replace(text, " VI ", "6", -1)
	text = strings.Replace(text, " V ", "5", -1)
	text = strings.Replace(text, " IV ", "4", -1)
	text = strings.Replace(text, " III ", "3", -1)
	text = strings.Replace(text, " II ", "2", -1)
	text = strings.Replace(text, " I ", "1", -1)

	return text
}

// Получаем массив(slice) с списком слов из строки со словами
func get_words_slice(text string) []string {
	text = strings.Trim(text, " ")

	// Заменяю все двойные пробелы на одинарные
	for strings.Contains(text, "  ") == true {
		text = strings.Replace(text, "  ", " ", -1)
	}

	result := strings.Split(text, " ")

	return result
}

// Удаление мусорных слов не несущих смысловую нагрузку
func remove_stop_words(text string) []string {
	result := make([]string, 0)

	array := [1517]string{"каждый", "лишь", "несмотря", "некоторые", "некоторый", "некоторая", "некоторое", "скольких", "сколькой", "чьими", "чьим", "поэтому", "вследствие", "вследствии", "послужило", "виде", "случае", "согласно", "помимо", "вчем", "поскольку", "оказалось", "обозначено", "называются", "называется", "называлось", "называлась", "назывался", "недавно", "случилось", "назовите", "является", "какую", "какой", "какова", "какого", "какими", "чьи", "чей", "чья", "чье", "чьё", "считается", "считался", "встречается", "почему", "обычно", "необходимо", "означает", "находится", "употребляется", "употребляются", "почему", "откуда", "вкаком", "какое", "чьему", "сколько", "каким", "каких", "какой", "каком", "какие", "какая", "каковы", "какому", "например", "ellos", "sin", "habida", "чуть", "dagli", "кем", "на", "anderen", "зачем", "unsere", "dieselben", "hubiese", "stesti", "сейчас", "ihnen", "into", "наших", "le", "hubieron", "esos", "algo", "yo", "eussiez", "dell", "hubiera", "gegen", "один", "vom", "больше", "tendréis", "eu", "tendría", "bis", "ими", "что", "х", "ею", "tutti", "fossi", "between", "будьте", "sea", "tendrías", "мое", "fueras", "foste", "да", "nostra", "welche", "avevi", "потому", "будто", "par", "fai", "раз", "dies", "nicht", "teniendo", "con", "des", "ours", "нашій", "para", "vos", "aveva", "dello", "un", "собой", "eran", "которыми", "era", "ayants", "yours", "tue", "then", "tenía", "allem", "solches", "который", "are", "э", "manchem", "sí", "mir", "нибудь", "hubiste", "she", "been", "давать", "eri", "mit", "чье", "своими", "als", "nella", "всю", "оно", "allen", "estáis", "нее", "alles", "е", "estuviste", "farete", "одну", "по", "étant", "auraient", "этих", "estuvieses", "чей", "facesse", "under", "tuviesen", "as", "mustn", "tuvieron", "of", "daß", "deinen", "tendríamos", "myself", "sulla", "serías", "musste", "estaría", "чья", "hubiesen", "в", "hin", "soyons", "нашу", "sia", "tendrían", "наш", "tra", "fusses", "ma", "farebbe", "всём", "serions", "вами", "имъ", "seront", "voi", "avessi", "anderes", "stavano", "por", "моем", "würde", "нет", "estuvieseis", "dagl", "estaré", "estaríamos", "которое", "чём", "ich", "ли", "machen", "какой", "ta", "своему", "такою", "был", "weiter", "es", "tuyos", "stette", "tuoi", "están", "estando", "anderer", "mío", "вам", "sarete", "habíais", "всею", "ad", "ещё", "selbst", "чтобы", "другого", "habidas", "avute", "самих", "aurez", "estaremos", "была", "ее", "eusse", "quienes", "suo", "вы", "saresti", "nor", "moi", "ко", "nach", "other", "und", "against", "mucho", "mi", "nel", "serais", "avreste", "il", "stareste", "наса", "своих", "потом", "einem", "estadas", "hayas", "nosotros", "моею", "eures", "ni", "gli", "ebbero", "fusse", "habremos", "miei", "много", "впрочем", "yourselves", "nostre", "estaríais", "sollte", "sur", "further", "нашему", "unser", "como", "what", "т", "algunas", "под", "their", "будем", "avrà", "sarei", "есть", "siete", "ist", "míos", "это", "hubimos", "dessen", "eurem", "only", "welcher", "же", "перед", "более", "était", "sera", "sentid", "свое", "estábamos", "tuve", "ya", "mancher", "sarebbero", "fus", "otras", "самим", "stessi", "одном", "estuvieran", "seras", "мою", "étantes", "being", "s", "sei", "tuvieses", "isn", "меня", "andern", "noch", "que", "das", "этим", "diese", "avrebbe", "también", "through", "nous", "я", "von", "своем", "fut", "hasn", "estuve", "his", "mais", "никогда", "будучи", "своей", "fueron", "mías", "all", "aveste", "могите", "ё", "мы", "suya", "nada", "vostro", "have", "aurais", "наша", "esas", "même", "едим", "за исключением", "ты", "ест", "nosotras", "abbiano", "tenga", "dieses", "jetzt", "über", "sommes", "dans", "tuviésemos", "estuviésemos", "nostro", "ander", "tengáis", "eussions", "c", "неё", "ce", "тією", "avrete", "eût", "jeder", "ihres", "dieselbe", "a", "mightn", "soll", "eûtes", "того", "можешь", "ті", "когда", "ti", "только", "starò", "sentidos", "alla", "auriez", "эту", "einiger", "siate", "how", "suyas", "faresti", "son", "este", "quelle", "hai", "own", "die", "tenían", "tenemos", "agli", "quello", "fuésemos", "вокруг", "чтоб", "сама", "entre", "del", "нашими", "e", "soy", "tuo", "тебе", "также", "sarò", "faceva", "muss", "haya", "fueseis", "te", "im", "étaient", "tendríais", "нашім", "diesem", "ellas", "keines", "seamos", "со", "sa", "sentida", "facciate", "for", "toi", "ci", "avuta", "eus", "n", "estuvisteis", "abbia", "denselben", "étants", "м", "vosostras", "el", "todos", "by", "suyo", "estaba", "г", "eso", "моими", "teníais", "одних", "там", "eut", "fuera", "lui", "diesen", "nello", "об", "tuvieran", "эти", "sentidas", "estaban", "мои", "au", "una", "just", "about", "jede", "einige", "dalle", "этого", "estarán", "ц", "вдруг", "dieser", "одно", "habías", "можем", "más", "tua", "down", "avions", "será", "man", "werden", "avresti", "starebbero", "can", "below", "fosti", "ю", "after", "dir", "hube", "hinter", "et", "fossero", "ces", "welchen", "non", "jene", "wieder", "sentido", "ihre", "tuyas", "it", "саму", "всегда", "давай", "didn", "facessi", "en", "этом", "одной", "during", "либо", "nun", "quanta", "sind", "sein", "tenéis", "estuvimos", "étante", "hemos", "stemmo", "моём", "tuviera", "тобою", "этими", "be", "томах", "bei", "estar", "os", "jener", "hay", "tuviese", "sugli", "lei", "теми", "которые", "won", "eurer", "quale", "уж", "our", "serez", "такие", "we", "eusses", "ella", "были", "sehr", "hab", "таком", "stando", "мне", "sul", "t", "off", "где", "нашою", "нему", "чем", "весь", "sus", "euren", "dallo", "л", "ai", "wollen", "ком", "могу", "mía", "du", "andere", "your", "fosse", "про", "vostra", "desselben", "negl", "здесь", "sobre", "any", "до", "во", "stanno", "no", "zwischen", "las", "etwas", "fuesen", "навсегда", "тою", "sullo", "was", "estén", "ebbi", "durante", "нашим", "vuestra", "avrò", "soyez", "внизу", "facevate", "estados", "sondern", "when", "estaréis", "estuvieron", "hasta", "моё", "нельзя", "solche", "with", "tes", "estos", "siano", "some", "р", "ayantes", "другой", "est", "col", "stavate", "eine", "su", "хотя", "se", "eines", "можно", "другие", "sue", "zwar", "the", "more", "почти", "do", "würden", "вдоль", "ела", "furono", "fecero", "она", "éramos", "contra", "avremo", "seréis", "pas", "needn", "re", "einer", "eûmes", "tendrás", "where", "fûmes", "tienen", "farei", "wir", "кажется", "aus", "лучше", "eres", "всем", "doesn", "questa", "будете", "tutto", "her", "auch", "avevate", "siente", "wouldn", "but", "то", "б", "её", "ihr", "los", "eravate", "is", "seas", "всему", "queste", "нею", "dann", "quella", "has", "noi", "will", "внутри", "dazu", "seré", "m", "ними", "сам", "тех", "ней", "вместо", "da", "j", "которую", "habrías", "too", "facemmo", "denn", "meiner", "hanno", "couldn", "sarai", "иметь", "моему", "nos", "та", "jedes", "tendré", "sulle", "derselben", "auf", "ohne", "seines", "к", "какая", "dove", "ante", "aurons", "erano", "будь", "votre", "starà", "eure", "все", "come", "sugl", "y", "ein", "tenidos", "ait", "без", "al", "habría", "meines", "war", "всех", "der", "нашої", "sua", "fuimos", "совсем", "not", "weil", "facevamo", "нем", "одними", "aurait", "habíamos", "qui", "tú", "у", "faccio", "saremmo", "welchem", "er", "seriez", "está", "наші", "soit", "мені", "such", "тоже", "solchen", "avrebbero", "seremos", "наше", "sich", "ы", "sean", "stava", "habido", "которая", "тот", "dall", "hayamos", "н", "емъ", "ihren", "ha", "pour", "don", "fanno", "sarà", "keiner", "on", "fui", "свою", "questi", "habrán", "ему", "most", "tengan", "estemos", "who", "эта", "seinen", "welches", "wollte", "starebbe", "они", "опять", "hadn", "damit", "aies", "aura", "ним", "avemmo", "кто", "те", "de", "кому", "indem", "soient", "avremmo", "habiendo", "because", "tenido", "étions", "eue", "delle", "dasselbe", "tuyo", "manchen", "stesse", "to", "разве", "которого", "degli", "stiano", "иногда", "hier", "faranno", "вниз", "куда", "does", "könnte", "такую", "estuvierais", "свой", "himself", "с", "тієї", "кого", "для", "hayáis", "свои", "sui", "une", "also", "vuestras", "nuestros", "than", "vostri", "could", "ayante", "ш", "seáis", "he", "stessero", "faceste", "shan", "unter", "hers", "ъ", "будет", "unseren", "может", "della", "тії", "могла", "моя", "хорошо", "habrían", "estéis", "furent", "up", "fûtes", "einen", "durch", "своего", "faccia", "тем", "per", "три", "оне", "viel", "them", "dort", "таким", "estarían", "contro", "ll", "ешь", "anderr", "zu", "hubiéramos", "while", "hubierais", "ont", "mis", "they", "aren", "д", "ведь", "nostri", "him", "наши", "самом", "которому", "nur", "моим", "fuiste", "зато", "farebbero", "einiges", "aber", "habríamos", "всё", "aux", "tuvo", "êtes", "ж", "собою", "stavo", "ту", "eurent", "herself", "doch", "такая", "starete", "над", "facciamo", "feci", "ero", "fareste", "о", "um", "которою", "fuéramos", "fummo", "моги", "kein", "avait", "мой", "hatten", "eues", "fuerais", "dai", "bist", "otra", "estarás", "à", "такому", "o", "из", "until", "самими", "è", "avevano", "alle", "nelle", "tus", "seríais", "seinem", "suis", "tenías", "как", "why", "keinem", "l", "avranno", "before", "am", "avesti", "avessimo", "had", "were", "if", "esa", "ничего", "avuti", "достаточно", "estada", "нашею", "estuviesen", "ayez", "todo", "auras", "sull", "можете", "habidos", "себе", "können", "и", "tiene", "leur", "einigem", "quante", "вот", "estuvo", "своею", "qué", "facevo", "aviez", "stia", "или", "cual", "между", "there", "saranno", "одному", "которым", "будешь", "после", "uno", "estuviera", "sto", "facendo", "конечно", "gewesen", "manche", "wo", "euch", "antes", "котором", "за", "тобой", "very", "которых", "quanto", "estés", "stettero", "sería", "которой", "avessero", "estoy", "ayons", "fussions", "tienes", "всего", "lo", "sie", "ем", "stavamo", "их", "теє", "вне", "avendo", "себя", "somos", "meinen", "своя", "eux", "dein", "нём", "mes", "algunos", "estas", "serás", "нами", "сами", "so", "jenen", "avete", "sareste", "vostre", "уже", "étés", "том", "qu", "hubisteis", "few", "faremo", "estuvieras", "ей", "самого", "unos", "keinen", "euer", "wasn", "eussent", "tuviste", "io", "tuvisteis", "sonst", "or", "fussiez", "farò", "yourself", "dich", "deiner", "отчего", "этой", "serons", "staranno", "siamo", "ese", "чего", "unseres", "такой", "ed", "теперь", "from", "nuestro", "bin", "themselves", "при", "vuestro", "avrai", "hat", "étiez", "cui", "нашем", "estará", "farai", "this", "estado", "at", "avevo", "während", "same", "sont", "serán", "out", "once", "quanti", "одни", "nell", "habéis", "было", "seine", "й", "нам", "tengas", "avevamo", "ses", "would", "starei", "auront", "самому", "ob", "sono", "тогда", "fue", "den", "facciano", "pero", "через", "тебя", "sta", "manches", "щ", "так", "вся", "whom", "avez", "donde", "fece", "esta", "vor", "hubieras", "tened", "and", "fuisteis", "notre", "anderm", "буду", "stetti", "stavi", "seiner", "serai", "in", "oder", "one", "solchem", "быть", "едят", "anderem", "всея", "that", "muchos", "nuestra", "i", "aie", "aient", "ч", "нашего", "habrás", "avrei", "tuvierais", "avec", "хоть", "такое", "poco", "weren", "estabais", "nuestras", "estarías", "jenes", "les", "facesti", "derer", "desde", "meinem", "fu", "ну", "mio", "derselbe", "aller", "ton", "you", "over", "tenida", "otro", "should", "hatte", "farà", "jedem", "erais", "таких", "мочь", "будут", "он", "shouldn", "above", "dei", "fuese", "такого", "tendremos", "quelli", "deinem", "staremmo", "otros", "habían", "faremmo", "ь", "такими", "anche", "ihrem", "keine", "having", "tendrá", "tuvimos", "снова", "haven", "hubieseis", "serait", "étées", "él", "je", "sois", "mia", "porque", "estás", "estuviese", "hubieran", "мною", "staresti", "coi", "нас", "него", "tendrán", "одного", "всеми", "могут", "più", "vosostros", "avons", "habe", "а", "seríamos", "estad", "meine", "sarebbe", "here", "did", "nei", "hayan", "fût", "tengamos", "perché", "комья", "estamos", "fossimo", "mon", "ain", "degl", "chi", "кроме", "li", "vi", "sintiendo", "estabas", "не", "mich", "этою", "demselben", "тому", "вас", "habríais", "negli", "werde", "serían", "eras", "wirst", "uns", "einigen", "warst", "своём", "jenem", "его", "tenidas", "этот", "однако", "deines", "dal", "wie", "ho", "fueses", "ел", "avuto", "них", "stiamo", "тут", "mí", "tuya", "ou", "saremo", "waren", "d", "ayant", "hubiésemos", "muy", "ins", "été", "which", "mein", "даже", "anders", "an", "abbiamo", "vous", "quien", "мной", "seraient", "наконец", "étais", "habré", "avaient", "tuvieseis", "hubo", "ve", "одна", "ihrer", "now", "той", "nichts", "от", "facevano", "cuando", "jeden", "tu", "всей", "facessimo", "ни", "aurai", "tengo", "своё", "weg", "fussent", "з", "eravamo", "dem", "my", "zum", "им", "haben", "одною", "stai", "очень", "wird", "если", "itself", "doing", "kann", "zur", "essendo", "han", "моего", "la", "unserem", "еще", "мог", "its", "questo", "чему", "habréis", "si", "étée", "stessimo", "могли", "ebbe", "esto", "само", "tanto", "моей", "надо", "teníamos", "suyos", "этому", "für", "facevi", "бы", "ф", "me", "ne", "stiate", "одним", "моих", "но", "estuviéramos", "fueran", "again", "staremo", "habrá", "esté", "elle", "suoi", "steste", "solcher", "tuviéramos", "di", "п", "ihm", "einig", "dalla", "both", "ihn", "своим", "facessero", "allo", "these", "abbiate", "hubieses", "tuvieras", "могло", "нашей", "starai", "avais", "each", "deine", "aurions", "theirs", "those", "ourselves", "mie", "che", "avesse", "había", "dov", "vuestros", "wenn", "loro", "agl", "два", "einmal"}

	// Получаем массив слов из строки
	sli := get_words_slice(text)

	for j := 0; j < len(sli); j++ {
		find := false
		for i := 0; i < len(array); i++ {

			if sli[j] == array[i] {
				find = true
				break
			}
		}

		if find == false {
			result = append(result, sli[j])
		}
	}

	return result
}

// Удаление дубликатов слов в массиве
func get_uniq_values(input []string) []string {
	// Если сделать это через map?
	result := make([]string, 0)
	words_count := make(map[string]int)

	for i := 0; i < len(input); i++ {
		words_count[input[i]] += 1
	}

	for i := 0; i < len(input); i++ {
		if words_count[input[i]] > 0 {
			result = append(result, input[i])
			words_count[input[i]] = 0
		}
	}

	return result
}

// ------ Получаем леммы ----------
// Возвращаем строку с леммами, общее кол-во лемм, леммы с качеством 0, 1 и 8
func get_lemmas_str(text string) (string, int, int, int, int) {
	lemmas_count := 0
	lemmas_quality_0 := 0
	lemmas_quality_1 := 0
	lemmas_quality_8 := 0

	// Удаляем сноски
	text = removeFootnote(text)
	text = remove_symbols(text)
	text = strings.ToLower(text)

	text_slice := remove_stop_words(text)
	text_slice = get_uniq_values(text_slice)

	result := make([]string, 0)

	for i := 0; i < len(text_slice); i++ {
		analyses := mystem.NewAnalyses(text_slice[i])
		defer analyses.Close()
		lemma := analyses.GetLemma(0)

		// Удаляем из текста все наречия и предлоги
		lemma_gram_slice := lemma.StemGram()
		must_remove_lemma := false

		if len(lemma_gram_slice) >= 1 {
			if lemma_gram_slice[0] == 129 || lemma_gram_slice[0] == 135 || lemma_gram_slice[0] == 139 || lemma_gram_slice[0] == 132 || lemma_gram_slice[0] == 134 || lemma_gram_slice[0] == 130 || lemma_gram_slice[0] == 131 || lemma_gram_slice[0] == 141 || lemma_gram_slice[0] == 140 {
				must_remove_lemma = true
			}
		}

		if must_remove_lemma == false {
			// Считаем кол-во лемм и качества лемм
			lemmas_count += 1
			lemma_quality := lemma.Quality()

			if lemma_quality == 0 {
				lemmas_quality_0 += 1
			} else if lemma_quality == 1 {
				lemmas_quality_1 += 1
			} else if lemma_quality == 8 {
				lemmas_quality_8 += 1
			}

			result = append(result, lemma.Text())
		}
	}

	var lemmas_str string

	// Если в тексте 0, 1 или 2 леммы, то добавляем фиктивные леммы, чтобы потом можно было сформировать би- три- граммы
	if len(result) <= 2 {
		if len(result) == 2 {
			lemmas_str = result[0] + " hkghjakagh"
		} else if len(result) == 1 {
			lemmas_str = result[0] + " zxcvbnm hkghjakagh"
		} else {
			lemmas_str = "akjjfaskhj zxcvbnm hkghjakagh"
		}
	} else {
		lemmas_str = strings.Join(result, " ")
	}

	return lemmas_str, lemmas_count, lemmas_quality_0, lemmas_quality_1, lemmas_quality_8
}

// Функция находит все общие уникальные леммы для параграфов и вопросов
func get_all_words() {
	// Храним в скольких параграфах/вопросах упоминалась данная лемма
	paragraph_counter := make(map[string]int)
	question_counter := make(map[string]int)

	// ---- Имя файла который обрабатываем ----
	filename := "train_task1_latest.csv"

	// Отмечаем что такой параграф/вопрос уже обрабатывался
	paragraph_ids := make(map[string]int)
	question_ids := make(map[string]int)

	dat, err := ioutil.ReadFile(filename)
	check(err)

	csv_str := string(dat)

	r := csv.NewReader(strings.NewReader(csv_str))

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	for i := 1; i < len(records); i++ {
		paragraph_id := records[i][0]
		question_id := records[i][1]

		// Обрабатываем леммы из параграфа
		if paragraph_ids[paragraph_id] == 0 {
			// Помечаем что такой параграф обработан
			paragraph_ids[paragraph_id] = 1

			// Получаем уникальные леммы из текста параграфа
			paragraph_lemmas_str, _, _, _, _ := get_lemmas_str(records[i][2])
			paragraph_lemmas_slice := get_words_slice(paragraph_lemmas_str)
			paragraph_lemmas_slice_uniq := get_uniq_values(paragraph_lemmas_slice)

			for j := 0; j < len(paragraph_lemmas_slice_uniq); j++ {
				paragraph_counter[paragraph_lemmas_slice_uniq[j]] += 1
			}
		}

		// Обрабатываем леммы из вопроса
		if question_ids[question_id] == 0 {
			// Помечаем что такой вопрос обработан
			question_ids[question_id] = 1

			// Получаем уникальные леммы из текста вопроса
			question_lemmas_str, _, _, _, _ := get_lemmas_str(records[i][3])
			question_lemmas_slice := get_words_slice(question_lemmas_str)
			question_lemmas_slice_uniq := get_uniq_values(question_lemmas_slice)

			for j := 0; j < len(question_lemmas_slice_uniq); j++ {
				question_counter[question_lemmas_slice_uniq[j]] += 1
			}
		}
	}

	// ---- Имя файла который обрабатываем ----
	filename = "test_task1_latest.csv"

	// Отмечаем что такой параграф/вопрос уже обрабатывался
	paragraph_ids = make(map[string]int)
	question_ids = make(map[string]int)

	dat, err = ioutil.ReadFile(filename)
	check(err)

	csv_str = string(dat)

	r = csv.NewReader(strings.NewReader(csv_str))

	records, err = r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	for i := 1; i < len(records); i++ {
		paragraph_id := records[i][0]
		question_id := records[i][1]

		// Обрабатываем леммы из параграфа
		if paragraph_ids[paragraph_id] == 0 {
			// Помечаем что такой параграф обработан
			paragraph_ids[paragraph_id] = 1

			// Получаем уникальные леммы из текста параграфа
			paragraph_lemmas_str, _, _, _, _ := get_lemmas_str(records[i][2])
			paragraph_lemmas_slice := get_words_slice(paragraph_lemmas_str)
			paragraph_lemmas_slice_uniq := get_uniq_values(paragraph_lemmas_slice)

			for j := 0; j < len(paragraph_lemmas_slice_uniq); j++ {
				paragraph_counter[paragraph_lemmas_slice_uniq[j]] += 1
			}
		}

		// Обрабатываем леммы из вопроса
		if question_ids[question_id] == 0 {
			// Помечаем что такой вопрос обработан
			question_ids[question_id] = 1

			// Получаем уникальные леммы из текста вопроса
			question_lemmas_str, _, _, _, _ := get_lemmas_str(records[i][3])
			question_lemmas_slice := get_words_slice(question_lemmas_str)
			question_lemmas_slice_uniq := get_uniq_values(question_lemmas_slice)

			for j := 0; j < len(question_lemmas_slice_uniq); j++ {
				question_counter[question_lemmas_slice_uniq[j]] += 1
			}
		}
	}

	// ---- Находим общие леммы для параграфов и вопросов ----
	for word_p, word_count_p := range paragraph_counter {
		if question_counter[word_p] > 0 {
			words[word_p] = word_count_p + question_counter[word_p]
		}
	}

	fmt.Println(len(words))
}

// Функция вычисляет все idfs для всех общих лемм встречающихся хотя бы по 1 разу в параграфе и вопросе
func get_idfs_words() {
	num_words := len(words)

	// Логарифм от кол-во уникальных параграфов / на кол-во параграфов в которых есть это слово
	for word_p, word_count_p := range words {
		idfs[word_p] = math.Log(float64(num_words) / float64(word_count_p))
	}
}

// Функция возвращает общие леммы из текста для всех параграфов и вопросов
func uniq_words_more_one(s []string) []string {
	result := make([]string, 0)

	for i := 0; i < len(s); i++ {
		if words[s[i]] > 0 {
			result = append(result, s[i])
		}
	}

	return result
}

// Функция для вычисления основных характеристик для обоих файлов train, test
func make_parameters(input string) {
	var filename string
	var filename_to_save string

	if input == "train" {
		filename = "train_task1_latest.csv"
	} else if input == "test" {
		filename = "test_task1_latest.csv"
	}

	dat, err := ioutil.ReadFile(filename)
	check(err)

	csv_str := string(dat)

	r := csv.NewReader(strings.NewReader(csv_str))

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	// Результат работы программы
	new_slice := make([][]string, 0)

	// Формируем заголовок
	element := make([]string, 0)

	element = append(element, strconv.Quote(records[0][0]))
	element = append(element, strconv.Quote(records[0][1]))
	element = append(element, strconv.Quote(records[0][2]))
	element = append(element, strconv.Quote(records[0][3]))

	if input == "train" {
		element = append(element, strconv.Quote(records[0][4])) // target
	}

	/*
		--- Параметры которые я переношу из Python ---
		1py. Кол-во уникальных лемм в параграфе и вопросе
		1.1py. % неуникальных лемм с качеством 0, 1 и 8

		2py. Кол-во общих уникальных лемм для параграфа и вопроса
		2.2py. Кол-во уникальных лемм в параграфе и вопросе, которые встречаются хотя бы раз в параграфах и хотя бы раз в вопросах
		2.3py Кол-во общих уникальных лемм для параграфа и вопроса, которые встречаются хотя бы раз в параграфах и хотя бы раз в вопросах
		2.4py Сумма логарифмов частоты для всех слов из параграфа/вопроса и общих лемм
		2.5py От 0-1 Процент сумма логарифмов общих лемм от максимальной суммы логарифмов лемм вопроса

		3py. Биграммы на основе лемм
		3.1py. Кол-во уникальных биграм-лемм в параграфе и вопросе
		3.2py. Кол-во общих уникальных биграм-лемм для параграфе и вопроса
		4py. % в виде 0.0-1.0 кол-во общих уникальных лемм / кол-во уникальных лемм вопроса
		5ру. % в виде 0.0-1.0 кол-во общих уникальных биграм-лемм / кол-во уникальных биграм-лемм вопроса

		6py. Триграммы на основе лемм
		6.1py Кол-во уникальных триграм-лемм в параграфе и вопросе
		6.2py Кол-во общих уникальных триграм-лемм для параграфе и вопроса
		6.3py % в виде 0.0-1.0 кол-во общих уникальных триграм-лемм / кол-во уникальных триграм-лемм вопроса
	*/

	element = append(element, strconv.Quote("paragraph_lemmas"))
	element = append(element, strconv.Quote("question_lemmas"))
	element = append(element, strconv.Quote("paragraph_lemmas_len"))
	element = append(element, strconv.Quote("question_lemmas_len"))

	element = append(element, strconv.Quote("paragraph_lemmas_quality_0_proc"))
	element = append(element, strconv.Quote("paragraph_lemmas_quality_1_proc"))
	element = append(element, strconv.Quote("paragraph_lemmas_quality_8_proc"))
	element = append(element, strconv.Quote("question_lemmas_quality_0_proc"))
	element = append(element, strconv.Quote("question_lemmas_quality_1_proc"))
	element = append(element, strconv.Quote("question_lemmas_quality_8_proc"))

	element = append(element, strconv.Quote("intersection_lemmas_len"))
	element = append(element, strconv.Quote("paragraph_lemmas_more_one_len"))
	element = append(element, strconv.Quote("question_lemmas_more_one_len"))
	element = append(element, strconv.Quote("intersection_lemmas_more_one_len"))

	element = append(element, strconv.Quote("idf_paragraph_lemmas_more_one"))
	element = append(element, strconv.Quote("idf_question_lemmas_more_one"))
	element = append(element, strconv.Quote("idf_intersection_lemmas_more_one"))
	element = append(element, strconv.Quote("idf_proc_question_lemmas_more_one"))

	element = append(element, strconv.Quote("paragraph_bigram_lemmas_len"))
	element = append(element, strconv.Quote("question_bigram_lemmas_len"))
	element = append(element, strconv.Quote("intersection_bigram_lemmas_len"))
	element = append(element, strconv.Quote("intersection_lemmas_proc"))
	element = append(element, strconv.Quote("intersection_bigram_lemmas_proc"))

	element = append(element, strconv.Quote("paragraph_trigram_lemmas_len"))
	element = append(element, strconv.Quote("question_trigram_lemmas_len"))
	element = append(element, strconv.Quote("intersection_trigram_lemmas_len"))
	element = append(element, strconv.Quote("intersection_trigram_lemmas_proc"))

	// Сбор дополнительной статистики по данным
	/* Что можно собрать:
	1. Количество символов в параграфе и вопросе (paragraph_chars_len, question_chars_len)
	2. Количество знаков препинания в параграфе и вопросе .,!?;:
	3. Кол-во слов в параграфе и вопросе
	4. Кол-во уникальных слов в параграфе и вопросе
	4a. Разность между Кол-вом слов в параграфе/вопросе и кол-вом уникальных слов в параграфе/вопросе
	5. Максимальная длина слова в параграфе и вопросе
	6. Является вопрос уникальным? Или он так же задан к какому-то параграфу. 1-уникальный, 0-неуникальный
	7. Если вопрос неуникальный, то сколько раз он был задан.
	8. Кол-во уникальных слов с большой буквы в вопросе. Есть теория, что в уникальных фейковых вопросах, в лучшем случае только
	первое слов написано с большой буквы.
	*/

	element = append(element, strconv.Quote("paragraph_chars_len"))
	element = append(element, strconv.Quote("question_chars_len"))
	element = append(element, strconv.Quote("paragraph_prep_symbols_count"))
	element = append(element, strconv.Quote("question_prep_symbols_count"))
	element = append(element, strconv.Quote("paragraph_words_count"))
	element = append(element, strconv.Quote("question_words_count"))
	element = append(element, strconv.Quote("paragraph_uniq_words_count"))
	element = append(element, strconv.Quote("question_uniq_words_count"))
	element = append(element, strconv.Quote("paragraph_words_delta"))
	element = append(element, strconv.Quote("question_words_delta"))
	element = append(element, strconv.Quote("max_paragraph_word_len"))
	element = append(element, strconv.Quote("max_question_word_len"))
	element = append(element, strconv.Quote("question_uniq"))
	element = append(element, strconv.Quote("question_count_in_text"))
	element = append(element, strconv.Quote("question_title_words_count"))

	// 6. Является вопрос уникальным? Или он так же задан к какому-то параграфу.
	// Сохраняем сколько раз встречается данный вопрос
	questions_count := make(map[string]int)

	for i := 0; i < len(records); i++ {
		questions_count[records[i][1]] += 1
	}

	new_slice = append(new_slice, element)

	// Обработка полученного текста
	for i := 1; i < len(records); i++ {
		element := make([]string, 0)

		// Здесь особенность, что в оригинальном тексте мы дубликаты слов вычистили, но преобразование уникальных слов в леммы
		// дает дубликаты лемм.
		// Например, питающиеся, питается, питаются -> питаться
		paragraph_lemmas_str, paragraph_lemmas_count, paragraph_lemmas_quality_0, paragraph_lemmas_quality_1, paragraph_lemmas_quality_8 := get_lemmas_str(records[i][2])
		question_lemmas_str, question_lemmas_count, question_lemmas_quality_0, question_lemmas_quality_1, question_lemmas_quality_8 := get_lemmas_str(records[i][3])

		element = append(element, strconv.Quote(records[i][0]))
		element = append(element, strconv.Quote(records[i][1]))
		element = append(element, strconv.Quote(records[i][2]))
		element = append(element, strconv.Quote(records[i][3]))
		if input == "train" {
			element = append(element, strconv.Quote(records[i][4])) // target
		}

		element = append(element, strconv.Quote(paragraph_lemmas_str))
		element = append(element, strconv.Quote(question_lemmas_str))

		paragraph_lemmas_slice := get_words_slice(paragraph_lemmas_str)
		question_lemmas_slice := get_words_slice(question_lemmas_str)

		// 1py. Кол-во уникальных лемм в параграфе и вопросе
		paragraph_lemmas_slice_uniq := get_uniq_values(paragraph_lemmas_slice)
		question_lemmas_slice_uniq := get_uniq_values(question_lemmas_slice)

		element = append(element, strconv.Quote(strconv.Itoa(len(paragraph_lemmas_slice_uniq))))
		element = append(element, strconv.Quote(strconv.Itoa(len(question_lemmas_slice_uniq))))

		// 1.1py. % неуникальных лемм с качеством 0, 1 и 8
		paragraph_lemmas_quality_0_proc := 0.0
		paragraph_lemmas_quality_1_proc := 0.0
		paragraph_lemmas_quality_8_proc := 0.0

		if paragraph_lemmas_count > 0 {
			paragraph_lemmas_quality_0_proc = toFixed(float64(paragraph_lemmas_quality_0)/float64(paragraph_lemmas_count), 2)
			paragraph_lemmas_quality_1_proc = toFixed(float64(paragraph_lemmas_quality_1)/float64(paragraph_lemmas_count), 2)
			paragraph_lemmas_quality_8_proc = toFixed(float64(paragraph_lemmas_quality_8)/float64(paragraph_lemmas_count), 2)
		}

		question_lemmas_quality_0_proc := 0.0
		question_lemmas_quality_1_proc := 0.0
		question_lemmas_quality_8_proc := 0.0

		if question_lemmas_count > 0 {
			question_lemmas_quality_0_proc = toFixed(float64(question_lemmas_quality_0)/float64(question_lemmas_count), 2)
			question_lemmas_quality_1_proc = toFixed(float64(question_lemmas_quality_1)/float64(question_lemmas_count), 2)
			question_lemmas_quality_8_proc = toFixed(float64(question_lemmas_quality_8)/float64(question_lemmas_count), 2)
		}

		element = append(element, strconv.Quote(strconv.FormatFloat(paragraph_lemmas_quality_0_proc, 'f', -1, 64)))
		element = append(element, strconv.Quote(strconv.FormatFloat(paragraph_lemmas_quality_1_proc, 'f', -1, 64)))
		element = append(element, strconv.Quote(strconv.FormatFloat(paragraph_lemmas_quality_8_proc, 'f', -1, 64)))
		element = append(element, strconv.Quote(strconv.FormatFloat(question_lemmas_quality_0_proc, 'f', -1, 64)))
		element = append(element, strconv.Quote(strconv.FormatFloat(question_lemmas_quality_1_proc, 'f', -1, 64)))
		element = append(element, strconv.Quote(strconv.FormatFloat(question_lemmas_quality_8_proc, 'f', -1, 64)))

		// 2py. Кол-во общих уникальных лемм для параграфа и вопроса
		intersection_lemmas_len := 0
		for j := 0; j < len(paragraph_lemmas_slice_uniq); j++ {
			for j0 := 0; j0 < len(question_lemmas_slice_uniq); j0++ {
				if paragraph_lemmas_slice_uniq[j] == question_lemmas_slice_uniq[j0] {
					intersection_lemmas_len += 1
				}
			}
		}

		element = append(element, strconv.Quote(strconv.Itoa(intersection_lemmas_len)))

		// 2.2py. Кол-во уникальных лемм в параграфе и вопросе, которые встречаются хотя бы раз в параграфах и хотя бы раз в вопросах
		paragraph_lemmas_more_one := uniq_words_more_one(paragraph_lemmas_slice_uniq)
		question_lemmas_more_one := uniq_words_more_one(question_lemmas_slice_uniq)

		element = append(element, strconv.Quote(strconv.Itoa(len(paragraph_lemmas_more_one))))
		element = append(element, strconv.Quote(strconv.Itoa(len(question_lemmas_more_one))))

		// 2.3py. Кол-во общих уникальных лемм для параграфа и вопроса, которые встречаются хотя бы раз в параграфах и хотя бы раз в вопросах
		intersection_lemmas_more_one := make([]string, 0)

		intersection_lemmas_more_one_len := 0
		for j := 0; j < len(paragraph_lemmas_more_one); j++ {
			for j0 := 0; j0 < len(question_lemmas_more_one); j0++ {
				if paragraph_lemmas_more_one[j] == question_lemmas_more_one[j0] {
					intersection_lemmas_more_one = append(intersection_lemmas_more_one, paragraph_lemmas_more_one[j])
					intersection_lemmas_more_one_len += 1
				}
			}
		}

		element = append(element, strconv.Quote(strconv.Itoa(intersection_lemmas_more_one_len)))

		// 2.4py Сумма логарифмов частоты для всех слов из параграфа/вопроса и общих лемм
		idf_paragraph_lemmas_more_one := 0.0
		for j := 0; j < len(paragraph_lemmas_more_one); j++ {
			idf_paragraph_lemmas_more_one += idfs[paragraph_lemmas_more_one[j]]
		}

		element = append(element, strconv.Quote(strconv.FormatFloat(idf_paragraph_lemmas_more_one, 'f', -1, 64)))

		idf_question_lemmas_more_one := 0.0
		for j := 0; j < len(question_lemmas_more_one); j++ {
			idf_question_lemmas_more_one += idfs[question_lemmas_more_one[j]]
		}

		element = append(element, strconv.Quote(strconv.FormatFloat(idf_question_lemmas_more_one, 'f', -1, 64)))

		idf_intersection_lemmas_more_one := 0.0
		for j := 0; j < len(intersection_lemmas_more_one); j++ {
			idf_intersection_lemmas_more_one += idfs[intersection_lemmas_more_one[j]]
		}

		element = append(element, strconv.Quote(strconv.FormatFloat(idf_intersection_lemmas_more_one, 'f', -1, 64)))

		// 2.5py От 0-1 Процент сумма логарифмов общих лемм от максимальной суммы логарифмов лемм вопроса
		idf_proc_question_lemmas_more_one := toFixed(0.0, 2)

		if idf_question_lemmas_more_one > 0 {
			idf_proc_question_lemmas_more_one = toFixed(idf_intersection_lemmas_more_one/idf_question_lemmas_more_one, 2)
		}

		element = append(element, strconv.Quote(strconv.FormatFloat(idf_proc_question_lemmas_more_one, 'f', -1, 64)))

		// 3py. Биграммы на основе лемм
		// Как решается проблема если в вопросе нет лемм?
		// Считаем что в вопросе всегда есть 2 леммы(Выше в коде мы это обеспечиваем с помощью фиктивных лемм)
		// Генерируем биграммы и потом оставляем уникальные
		paragraph_bigram_lemmas_slice := make([]string, 0)
		question_bigram_lemmas_slice := make([]string, 0)

		for j := 0; j < len(paragraph_lemmas_slice)-1; j++ {
			paragraph_bigram_lemmas_slice = append(paragraph_bigram_lemmas_slice, paragraph_lemmas_slice[j]+paragraph_lemmas_slice[j+1])
		}

		for j := 0; j < len(question_lemmas_slice)-1; j++ {
			question_bigram_lemmas_slice = append(question_bigram_lemmas_slice, question_lemmas_slice[j]+question_lemmas_slice[j+1])
		}

		paragraph_bigram_lemmas_slice_uniq := get_uniq_values(paragraph_bigram_lemmas_slice)
		question_bigram_lemmas_slice_uniq := get_uniq_values(question_bigram_lemmas_slice)

		// 3.1py
		element = append(element, strconv.Quote(strconv.Itoa(len(paragraph_bigram_lemmas_slice_uniq))))
		element = append(element, strconv.Quote(strconv.Itoa(len(question_bigram_lemmas_slice_uniq))))

		// 3.2py
		intersection_bigram_lemmas_len := 0
		for j := 0; j < len(paragraph_bigram_lemmas_slice_uniq); j++ {
			for j0 := 0; j0 < len(question_bigram_lemmas_slice_uniq); j0++ {
				if paragraph_bigram_lemmas_slice_uniq[j] == question_bigram_lemmas_slice_uniq[j0] {
					intersection_bigram_lemmas_len += 1
				}
			}
		}

		element = append(element, strconv.Quote(strconv.Itoa(intersection_bigram_lemmas_len)))

		// 4py. % в виде 0.0-1.0 кол-во общих уникальных лемм / кол-во уникальных лемм вопроса
		intersection_lemmas_proc := toFixed(float64(intersection_lemmas_len)/float64(len(question_lemmas_slice_uniq)), 2)

		element = append(element, strconv.Quote(strconv.FormatFloat(intersection_lemmas_proc, 'f', -1, 64)))

		// 5ру. % в виде 0.0-1.0 кол-во общих уникальных биграм-лемм / кол-во уникальных биграм-лемм вопроса
		intersection_bigram_lemmas_proc := toFixed(float64(intersection_bigram_lemmas_len)/float64(len(question_bigram_lemmas_slice_uniq)), 2)

		element = append(element, strconv.Quote(strconv.FormatFloat(intersection_bigram_lemmas_proc, 'f', -1, 64)))

		// 6.1py Кол-во уникальных триграм-лемм в параграфе и вопросе
		paragraph_trigram_lemmas_slice := make([]string, 0)
		question_trigram_lemmas_slice := make([]string, 0)

		for j := 0; j < len(paragraph_lemmas_slice)-2; j++ {
			paragraph_trigram_lemmas_slice = append(paragraph_trigram_lemmas_slice, paragraph_lemmas_slice[j]+paragraph_lemmas_slice[j+1]+paragraph_lemmas_slice[j+2])
		}

		for j := 0; j < len(question_lemmas_slice)-2; j++ {
			question_trigram_lemmas_slice = append(question_trigram_lemmas_slice, question_lemmas_slice[j]+question_lemmas_slice[j+1]+question_lemmas_slice[j+2])
		}

		paragraph_trigram_lemmas_slice_uniq := get_uniq_values(paragraph_trigram_lemmas_slice)
		question_trigram_lemmas_slice_uniq := get_uniq_values(question_trigram_lemmas_slice)

		element = append(element, strconv.Quote(strconv.Itoa(len(paragraph_trigram_lemmas_slice_uniq))))
		element = append(element, strconv.Quote(strconv.Itoa(len(question_trigram_lemmas_slice_uniq))))

		// 6.2py Кол-во общих уникальных триграм-лемм для параграфе и вопроса
		intersection_trigram_lemmas_len := 0
		for j := 0; j < len(paragraph_trigram_lemmas_slice_uniq); j++ {
			for j0 := 0; j0 < len(question_trigram_lemmas_slice_uniq); j0++ {
				if paragraph_trigram_lemmas_slice_uniq[j] == question_trigram_lemmas_slice_uniq[j0] {
					intersection_trigram_lemmas_len += 1
				}
			}
		}

		element = append(element, strconv.Quote(strconv.Itoa(intersection_trigram_lemmas_len)))

		// 6.3py % в виде 0.0-1.0 кол-во общих уникальных триграм-лемм / кол-во уникальных триграм-лемм вопроса
		intersection_trigram_lemmas_proc := toFixed(float64(intersection_trigram_lemmas_len)/float64(len(question_trigram_lemmas_slice_uniq)), 2)

		element = append(element, strconv.Quote(strconv.FormatFloat(intersection_trigram_lemmas_proc, 'f', -1, 64)))

		// --- Сбор дополнительной статистики ---
		paragraph := records[i][2]
		question := records[i][3]

		// 1. Кол-во символов в параграфе и вопросе
		paragraph_chars_len := len(paragraph)
		question_chars_len := len(question)

		element = append(element, strconv.Quote(strconv.Itoa(paragraph_chars_len)))
		element = append(element, strconv.Quote(strconv.Itoa(question_chars_len)))

		// 2. Кол-во знаков препинания в параграфе и вопросе
		prep_symbols := [6]string{".", ",", "!", "?", ":", ";"}

		paragraph_prep_symbols_count := 0
		question_prep_symbols_count := 0

		for j := 0; j < len(prep_symbols); j++ {
			paragraph_prep_symbols_count += strings.Count(paragraph, prep_symbols[j])
			question_prep_symbols_count += strings.Count(question, prep_symbols[j])
		}

		element = append(element, strconv.Quote(strconv.Itoa(paragraph_prep_symbols_count)))
		element = append(element, strconv.Quote(strconv.Itoa(question_prep_symbols_count)))

		// 3. Кол-во слов в параграфе и вопросе
		text := strings.Trim(paragraph, " ") // Убираю пробелы в начале и конце текста

		// Заменяю все двойные пробелы на одинарные
		for strings.Contains(text, "  ") == true {
			text = strings.Replace(text, "  ", " ", -1)
		}

		paragraph_words := strings.Split(text, " ")
		paragraph_words_count := len(paragraph_words)

		text = strings.Trim(question, " ") // Убираю пробелы в начале и конце текста

		// Заменяю все двойные пробелы на одинарные
		for strings.Contains(text, "  ") == true {
			text = strings.Replace(text, "  ", " ", -1)
		}

		question_words := strings.Split(text, " ")
		question_words_count := len(question_words)

		element = append(element, strconv.Quote(strconv.Itoa(paragraph_words_count)))
		element = append(element, strconv.Quote(strconv.Itoa(question_words_count)))

		// 4. Кол-во уникальных слов в параграфе и вопросе
		paragraph_uniq_words := get_uniq_values(paragraph_words)
		question_uniq_words := get_uniq_values(question_words)

		paragraph_uniq_words_count := len(paragraph_uniq_words)
		question_uniq_words_count := len(question_uniq_words)

		element = append(element, strconv.Quote(strconv.Itoa(paragraph_uniq_words_count)))
		element = append(element, strconv.Quote(strconv.Itoa(question_uniq_words_count)))

		// 4a. Разность между Кол-вом слов в параграфе/вопросе и кол-вом уникальных слов в параграфе/вопросе
		paragraph_words_delta := paragraph_words_count - paragraph_uniq_words_count
		question_words_delta := question_words_count - question_uniq_words_count

		element = append(element, strconv.Quote(strconv.Itoa(paragraph_words_delta)))
		element = append(element, strconv.Quote(strconv.Itoa(question_words_delta)))

		// 5. Максимальная длина слова в параграфе и вопросе
		max_paragraph_word_len := 0
		for j := 0; j < len(paragraph_uniq_words); j++ {
			if len(paragraph_uniq_words[j]) > max_paragraph_word_len {
				max_paragraph_word_len = len(paragraph_uniq_words[j])
			}
		}

		max_question_word_len := 0
		for j := 0; j < len(question_uniq_words); j++ {
			if len(question_uniq_words[j]) > max_question_word_len {
				max_question_word_len = len(question_uniq_words[j])
			}
		}

		element = append(element, strconv.Quote(strconv.Itoa(max_paragraph_word_len)))
		element = append(element, strconv.Quote(strconv.Itoa(max_question_word_len)))

		// 6. Является вопрос уникальным? Или он так же задан к какому-то параграфу. 1-уникальный, 0-неуникальный
		// 7. Если вопрос неуникальный, то сколько раз он был задан.
		if questions_count[records[i][1]] == 1 {
			element = append(element, strconv.Quote("1"))
		} else {
			element = append(element, strconv.Quote("0"))
		}
		element = append(element, strconv.Quote(strconv.Itoa(questions_count[records[i][1]])))

		// 8. Кол-во уникальных слов с большой буквы в вопросе
		question_title_words_count := 0

		for j := 0; j < len(question_uniq_words); j++ {
			if question_uniq_words[j] == strings.Title(question_uniq_words[j]) {
				question_title_words_count += 1
			}
		}

		element = append(element, strconv.Quote(strconv.Itoa(question_title_words_count)))

		// Добавляем новый элемент в массив
		new_slice = append(new_slice, element)

		// Смотрим не надо ли этот элемент занести из test в train?
		if input == "test" {
			if questions_count[records[i][1]] == 1 {
				// 22766
				if question_prep_symbols_count >= 3 || question_words_delta >= 1 || question_lemmas_quality_8_proc >= 0.3 || question_lemmas_quality_1_proc >= 0.15 || len(question_lemmas_slice_uniq) >= 16 {
					test_to_train_0 = append(test_to_train_0, element)
				}

				// 1278
				if idf_proc_question_lemmas_more_one == 1 && intersection_bigram_lemmas_proc == 1 {
					if question_prep_symbols_count < 3 && question_words_delta == 0 && question_lemmas_quality_8_proc < 0.3 && len(question_lemmas_slice_uniq) < 16 {
						test_to_train_1 = append(test_to_train_1, element)
					}
				}
			}
		}

		fmt.Println(i)
	}

	// Добавляем дополнительные данные из test в train
	if input == "train" {
		test_to_train_element_len := len(test_to_train_0[0])

		for i := 0; i < len(test_to_train_0); i++ {
			element := make([]string, 0)

			for j := 0; j < test_to_train_element_len; j++ {
				element = append(element, test_to_train_0[i][j])
				if j == 3 {
					element = append(element, "0.0")
				}
			}

			new_slice = append(new_slice, element)
		}
		/**/
		test_to_train_element_len = len(test_to_train_1[0])

		for i := 0; i < len(test_to_train_1); i++ {
			element := make([]string, 0)

			for j := 0; j < test_to_train_element_len; j++ {
				element = append(element, test_to_train_1[i][j])
				if j == 3 {
					element = append(element, "1.0")
				}
			}

			new_slice = append(new_slice, element)
		}
		/**/
	}

	// Пишем в csv-файл
	if input == "train" {
		filename_to_save = "train_task1_latest_my.csv"
	} else if input == "test" {
		filename_to_save = "test_task1_latest_my.csv"
	}

	file, err := os.Create(filename_to_save)
	checkError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range new_slice {
		err := writer.Write(value)
		checkError("Cannot write to file", err)
	}
}

var words = map[string]int{}
var idfs = map[string]float64{}
var test_to_train_0 = [][]string{}
var test_to_train_1 = [][]string{}

func main() {
	get_all_words()
	get_idfs_words()

	make_parameters("test")
	make_parameters("train")
}
