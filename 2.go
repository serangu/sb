package main

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	_ "regexp"
	"strings"

	"github.com/dveselov/mystem"
)

// Обработка ошибок
func check(e error) {
	if e != nil {
		panic(e)
	}
}
func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

// Пара функций для округления результатов
func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

// Удаление сносок [xxx], если они есть и удаление всех символов
// Так же удаление сносок вида [источник не указан 589 дней]
func removeFootnote(text string) string {
	var start, finish, delta int

	for strings.Contains(text, "[") == true {
		start = strings.Index(text, "[")
		finish = strings.Index(text, "]")

		if start == -1 || finish == -1 {
			return text
		}

		delta = finish - start

		// Подозреваем что это сноска
		if delta >= 1 && delta <= 3 {
			//fmt.Println(text[start : finish+1])
			text = text[:start] + text[finish+1:]
		} else {
			start_long_0 := strings.Index(text, "[источник не указан")
			start_long_1 := strings.Index(text, "[уточните ссылку")
			start_long_2 := strings.Index(text, "[страница не указана")

			if start == start_long_0 || start == start_long_1 || start == start_long_2 {
				//fmt.Println(text[start : finish+1])
				text = text[:start] + text[finish+1:]
			} else {
				text = strings.Replace(text, "[", " ", 1)
				text = strings.Replace(text, "]", " ", 1)
			}
		}
	}

	return text
}

// Удаление знаков препинания
func remove_symbols(text string) string {
	text = strings.Replace(text, "1ntel", "intel", -1)
	text = strings.Replace(text, "“", " ", -1)
	text = strings.Replace(text, "„", " ", -1)
	text = strings.Replace(text, "1nitiative", "initiative", -1)
	text = strings.Replace(text, "⇧", " ", -1)
	text = strings.Replace(text, "1so", "iso", -1)
	text = strings.Replace(text, "1ml", "1 ml", -1)
	text = strings.Replace(text, "°", " градус ", -1)
	text = strings.Replace(text, "′", " ", -1)

	text = strings.Replace(text, "женнсие", "женские", -1)
	text = strings.Replace(text, "предстиавлены", "представлены", -1)
	text = strings.Replace(text, "ктосчитает", "кто считает", -1)
	text = strings.Replace(text, "тыс.", "тысяч ", -1)
	text = strings.Replace(text, "(i)", "", -1)
	text = strings.Replace(text, "облигакции", "облигации", -1)
	text = strings.Replace(text, "анлиз", "анализ", -1)
	text = strings.Replace(text, "л. с.", " лошадиных сил ", -1)
	text = strings.Replace(text, "ЦБ", "центральный банк", -1)
	text = strings.Replace(text, "РФ", "Российская федерация россия", -1)
	text = strings.Replace(text, " рф ", " Российская федерация россия ", -1)
	text = strings.Replace(text, "…", " ", -1)
	text = strings.Replace(text, "ст.-сл.", "старо славянский", -1)
	text = strings.Replace(text, "ц.-сл.", "церковно славянский", -1)
	text = strings.Replace(text, "€", " евро ", -1)
	text = strings.Replace(text, "к. н.", "кандидат наук", -1)
	text = strings.Replace(text, "н. э.", "нашей эры", -1)
	text = strings.Replace(text, "нем.", "", -1)
	text = strings.Replace(text, "араб.", "", -1)
	text = strings.Replace(text, "см.", "", -1)
	text = strings.Replace(text, "фр.", "", -1)
	text = strings.Replace(text, "др.-греч.", "", -1)
	text = strings.Replace(text, "греч.", "", -1)
	text = strings.Replace(text, "лат.", "", -1)
	text = strings.Replace(text, "англ.", "", -1)
	text = strings.Replace(text, "\u00ad", "", -1)
	text = strings.Replace(text, "рус.", "", -1)
	text = strings.Replace(text, "итал.", "", -1)
	text = strings.Replace(text, "франц.", "", -1)
	text = strings.Replace(text, "см.", "", -1)
	text = strings.Replace(text, "²", " площадь ", -1)
	text = strings.Replace(text, "р.", " рубли ", -1)
	text = strings.Replace(text, "руб ", " рубли ", -1)
	text = strings.Replace(text, "млрд", " миллиард ", -1)
	text = strings.Replace(text, "’s", "", -1)
	text = strings.Replace(text, "ирл.", "", -1)
	text = strings.Replace(text, "местн.", "", -1)
	text = strings.Replace(text, "т.", " тонн ", -1)
	text = strings.Replace(text, "·", "", -1)
	text = strings.Replace(text, "м/с", " метров в секунду скорость ", -1)
	text = strings.Replace(text, "ст.", " статья ", -1)
	text = strings.Replace(text, "pH", " кислотность ", -1)
	text = strings.Replace(text, "°C", " градус ", -1)
	text = strings.Replace(text, "%", " процент ", -1)
	text = strings.Replace(text, "е́", "е", -1)
	text = strings.Replace(text, "é", "e", -1)
	text = strings.Replace(text, "а́", "а", -1)
	text = strings.Replace(text, "о́", "о", -1)
	text = strings.Replace(text, "и́", "и", -1)
	text = strings.Replace(text, ".", " ", -1)
	text = strings.Replace(text, ",", " ", -1)
	text = strings.Replace(text, "!", " ", -1)
	text = strings.Replace(text, "_", " ", -1)
	text = strings.Replace(text, "~", " ", -1)
	text = strings.Replace(text, "?", " ", -1)
	text = strings.Replace(text, "(", " ", -1)
	text = strings.Replace(text, ")", " ", -1)
	text = strings.Replace(text, "/", " ", -1)
	text = strings.Replace(text, "\\", "", -1)
	text = strings.Replace(text, "-м", "", -1)
	text = strings.Replace(text, "-е", "", -1)
	text = strings.Replace(text, "-х", "", -1)
	text = strings.Replace(text, "-x", "", -1)
	text = strings.Replace(text, "[", " ", -1)
	text = strings.Replace(text, "]", " ", -1)
	text = strings.Replace(text, ":", " ", -1)
	text = strings.Replace(text, "+", " ", -1)
	text = strings.Replace(text, "*", " ", -1)
	text = strings.Replace(text, "&", " ", -1)
	text = strings.Replace(text, "^", " ", -1)
	text = strings.Replace(text, ";", " ", -1)
	text = strings.Replace(text, "'", " ", -1)
	text = strings.Replace(text, "\"", " ", -1)
	text = strings.Replace(text, "-", " ", -1)
	text = strings.Replace(text, "—", " ", -1)
	text = strings.Replace(text, "=", " равно ", -1)
	text = strings.Replace(text, "№", " номер ", -1)
	text = strings.Replace(text, "#", " номер ", -1)
	text = strings.Replace(text, "СПб", "Санкт Петербург", -1)
	text = strings.Replace(text, "МСК", "Москва", -1)
	text = strings.Replace(text, "ДВС", "двигатель внутренного сгорания", -1)
	text = strings.Replace(text, "гг", " годах ", -1)
	text = strings.Replace(text, "ФЗ", " федеральный закон ", -1)
	text = strings.Replace(text, "УГ", " уголовный кодекс ", -1)
	text = strings.Replace(text, "млн", "миллион", -1)
	text = strings.Replace(text, "США", "Соединенные Штаты Америки", -1)
	text = strings.Replace(text, "€", " евро ", -1)
	text = strings.Replace(text, "$", " доллар ", -1)
	text = strings.Replace(text, "Ё", "Е", -1)
	text = strings.Replace(text, "ё", "е", -1)
	text = strings.Replace(text, "XXV", "25", -1)
	text = strings.Replace(text, "XXIV", "24", -1)
	text = strings.Replace(text, "XXIII", "23", -1)
	text = strings.Replace(text, "XXII", "22", -1)
	text = strings.Replace(text, "XXI", "21", -1)
	text = strings.Replace(text, "XX", "20", -1)
	text = strings.Replace(text, "XIX", "19", -1)
	text = strings.Replace(text, "XVIII", "18", -1)
	text = strings.Replace(text, "XVII", "17", -1)
	text = strings.Replace(text, "XVI", "16", -1)
	text = strings.Replace(text, "XV", "15", -1)
	text = strings.Replace(text, "XIV", "14", -1)
	text = strings.Replace(text, "XIII", "13", -1)
	text = strings.Replace(text, "XII", "12", -1)
	text = strings.Replace(text, "XI", "11", -1)
	text = strings.Replace(text, "X", "10", -1)
	text = strings.Replace(text, "IX", "9", -1)
	text = strings.Replace(text, "VIII", "8", -1)
	text = strings.Replace(text, "VII", "7", -1)
	text = strings.Replace(text, "VI", "6", -1)
	text = strings.Replace(text, "V", "5", -1)
	text = strings.Replace(text, "IV", "4", -1)
	text = strings.Replace(text, "III", "3", -1)
	text = strings.Replace(text, "II", "2", -1)
	text = strings.Replace(text, "I", "1", -1)

	return text
}

// Получаем массив(slice) с списком слов из строки со словами
func get_words_slice(text string) []string {
	text = strings.Trim(text, " ")

	// Заменяю все двойные пробелы на одинарные
	for strings.Contains(text, "  ") == true {
		text = strings.Replace(text, "  ", " ", -1)
	}

	result := strings.Split(text, " ")

	return result
}

// Удаление мусорных слов не несущих смысловую нагрузку
func remove_stop_words(text string) []string {
	result := make([]string, 0)

	array := [1543]string{"определить", "определяется", "каждый", "происходить", "происходит", "произошло", "случилось", "представляет", "представляют", "предлагает", "предлагают", "зовут", "звали", "определением", "понимаются", "понимается", "заключаются", "заключается", "лишь", "ознаменовало", "ознаменовал", "ознаменовала", "ознаменовали", "приведите", "пример", "примеры", "свидетельствует", "словам", "несмотря", "некоторые", "некоторый", "некоторая", "некоторое", "скольких", "сколькой", "чьими", "чьим", "поэтому", "вследствие", "вследствии", "послужило", "виде", "случае", "согласно", "помимо", "вчем", "поскольку", "оказалось", "обозначено", "называются", "называется", "называлось", "называлась", "назывался", "недавно", "случилось", "назовите", "является", "какую", "какой", "какова", "какого", "какими", "чьи", "чей", "чья", "чье", "чьё", "считается", "считался", "встречается", "почему", "обычно", "необходимо", "означает", "находится", "употребляется", "употребляются", "почему", "откуда", "вкаком", "какое", "чьему", "сколько", "каким", "каких", "какой", "каком", "какие", "какая", "каковы", "какому", "например", "ellos", "sin", "habida", "чуть", "dagli", "кем", "на", "anderen", "зачем", "unsere", "dieselben", "hubiese", "stesti", "сейчас", "ihnen", "into", "наших", "le", "hubieron", "esos", "algo", "yo", "eussiez", "dell", "hubiera", "gegen", "один", "vom", "больше", "tendréis", "eu", "tendría", "bis", "ими", "что", "х", "ею", "tutti", "fossi", "between", "будьте", "sea", "tendrías", "мое", "fueras", "foste", "да", "nostra", "welche", "avevi", "потому", "будто", "par", "fai", "раз", "dies", "nicht", "teniendo", "con", "des", "ours", "нашій", "para", "vos", "aveva", "dello", "un", "собой", "eran", "которыми", "era", "ayants", "yours", "tue", "then", "tenía", "allem", "solches", "который", "are", "э", "manchem", "sí", "mir", "нибудь", "hubiste", "she", "been", "давать", "eri", "mit", "чье", "своими", "als", "nella", "всю", "оно", "allen", "estáis", "нее", "alles", "е", "estuviste", "farete", "одну", "по", "étant", "auraient", "этих", "estuvieses", "чей", "facesse", "under", "tuviesen", "as", "mustn", "tuvieron", "of", "daß", "deinen", "tendríamos", "myself", "sulla", "serías", "musste", "estaría", "чья", "hubiesen", "в", "hin", "soyons", "нашу", "sia", "tendrían", "наш", "tra", "fusses", "ma", "farebbe", "всём", "serions", "вами", "имъ", "seront", "voi", "avessi", "anderes", "stavano", "por", "моем", "würde", "нет", "estuvieseis", "dagl", "estaré", "estaríamos", "которое", "чём", "ich", "ли", "machen", "какой", "ta", "своему", "такою", "был", "weiter", "es", "tuyos", "stette", "tuoi", "están", "estando", "anderer", "mío", "вам", "sarete", "habíais", "всею", "ad", "ещё", "selbst", "чтобы", "другого", "habidas", "avute", "самих", "aurez", "estaremos", "была", "ее", "eusse", "quienes", "suo", "вы", "saresti", "nor", "moi", "ко", "nach", "other", "und", "against", "mucho", "mi", "nel", "serais", "avreste", "il", "stareste", "наса", "своих", "потом", "einem", "estadas", "hayas", "nosotros", "моею", "eures", "ni", "gli", "ebbero", "fusse", "habremos", "miei", "много", "впрочем", "yourselves", "nostre", "estaríais", "sollte", "sur", "further", "нашему", "unser", "como", "what", "т", "algunas", "под", "their", "будем", "avrà", "sarei", "есть", "siete", "ist", "míos", "это", "hubimos", "dessen", "eurem", "only", "welcher", "же", "перед", "более", "était", "sera", "sentid", "свое", "estábamos", "tuve", "ya", "mancher", "sarebbero", "fus", "otras", "самим", "stessi", "одном", "estuvieran", "seras", "мою", "étantes", "being", "s", "sei", "tuvieses", "isn", "меня", "andern", "noch", "que", "das", "этим", "diese", "avrebbe", "también", "through", "nous", "я", "von", "своем", "fut", "hasn", "estuve", "his", "mais", "никогда", "будучи", "своей", "fueron", "mías", "all", "aveste", "могите", "ё", "мы", "suya", "nada", "vostro", "have", "aurais", "наша", "esas", "même", "едим", "за исключением", "ты", "ест", "nosotras", "abbiano", "tenga", "dieses", "jetzt", "über", "sommes", "dans", "tuviésemos", "estuviésemos", "nostro", "ander", "tengáis", "eussions", "c", "неё", "ce", "тією", "avrete", "eût", "jeder", "ihres", "dieselbe", "a", "mightn", "soll", "eûtes", "того", "можешь", "ті", "когда", "ti", "только", "starò", "sentidos", "alla", "auriez", "эту", "einiger", "siate", "how", "suyas", "faresti", "son", "este", "quelle", "hai", "own", "die", "tenían", "tenemos", "agli", "quello", "fuésemos", "вокруг", "чтоб", "сама", "entre", "del", "нашими", "e", "soy", "tuo", "тебе", "также", "sarò", "faceva", "muss", "haya", "fueseis", "te", "im", "étaient", "tendríais", "нашім", "diesem", "ellas", "keines", "seamos", "со", "sa", "sentida", "facciate", "for", "toi", "ci", "avuta", "eus", "n", "estuvisteis", "abbia", "denselben", "étants", "м", "vosostras", "el", "todos", "by", "suyo", "estaba", "г", "eso", "моими", "teníais", "одних", "там", "eut", "fuera", "lui", "diesen", "nello", "об", "tuvieran", "эти", "sentidas", "estaban", "мои", "au", "una", "just", "about", "jede", "einige", "dalle", "этого", "estarán", "ц", "вдруг", "dieser", "одно", "habías", "можем", "más", "tua", "down", "avions", "será", "man", "werden", "avresti", "starebbero", "can", "below", "fosti", "ю", "after", "dir", "hube", "hinter", "et", "fossero", "ces", "welchen", "non", "jene", "wieder", "sentido", "ihre", "tuyas", "it", "саму", "всегда", "давай", "didn", "facessi", "en", "этом", "одной", "during", "либо", "nun", "quanta", "sind", "sein", "tenéis", "estuvimos", "étante", "hemos", "stemmo", "моём", "tuviera", "тобою", "этими", "be", "томах", "bei", "estar", "os", "jener", "hay", "tuviese", "sugli", "lei", "теми", "которые", "won", "eurer", "quale", "уж", "our", "serez", "такие", "we", "eusses", "ella", "были", "sehr", "hab", "таком", "stando", "мне", "sul", "t", "off", "где", "нашою", "нему", "чем", "весь", "sus", "euren", "dallo", "л", "ai", "wollen", "ком", "могу", "mía", "du", "andere", "your", "fosse", "про", "vostra", "desselben", "negl", "здесь", "sobre", "any", "до", "во", "stanno", "no", "zwischen", "las", "etwas", "fuesen", "навсегда", "тою", "sullo", "was", "estén", "ebbi", "durante", "нашим", "vuestra", "avrò", "soyez", "внизу", "facevate", "estados", "sondern", "when", "estaréis", "estuvieron", "hasta", "моё", "нельзя", "solche", "with", "tes", "estos", "siano", "some", "р", "ayantes", "другой", "est", "col", "stavate", "eine", "su", "хотя", "se", "eines", "можно", "другие", "sue", "zwar", "the", "more", "почти", "do", "würden", "вдоль", "ела", "furono", "fecero", "она", "éramos", "contra", "avremo", "seréis", "pas", "needn", "re", "einer", "eûmes", "tendrás", "where", "fûmes", "tienen", "farei", "wir", "кажется", "aus", "лучше", "eres", "всем", "doesn", "questa", "будете", "tutto", "her", "auch", "avevate", "siente", "wouldn", "but", "то", "б", "её", "ihr", "los", "eravate", "is", "seas", "всему", "queste", "нею", "dann", "quella", "has", "noi", "will", "внутри", "dazu", "seré", "m", "ними", "сам", "тех", "ней", "вместо", "da", "j", "которую", "habrías", "too", "facemmo", "denn", "meiner", "hanno", "couldn", "sarai", "иметь", "моему", "nos", "та", "jedes", "tendré", "sulle", "derselben", "auf", "ohne", "seines", "к", "какая", "dove", "ante", "aurons", "erano", "будь", "votre", "starà", "eure", "все", "come", "sugl", "y", "ein", "tenidos", "ait", "без", "al", "habría", "meines", "war", "всех", "der", "нашої", "sua", "fuimos", "совсем", "not", "weil", "facevamo", "нем", "одними", "aurait", "habíamos", "qui", "tú", "у", "faccio", "saremmo", "welchem", "er", "seriez", "está", "наші", "soit", "мені", "such", "тоже", "solchen", "avrebbero", "seremos", "наше", "sich", "ы", "sean", "stava", "habido", "которая", "тот", "dall", "hayamos", "н", "емъ", "ihren", "ha", "pour", "don", "fanno", "sarà", "keiner", "on", "fui", "свою", "questi", "habrán", "ему", "most", "tengan", "estemos", "who", "эта", "seinen", "welches", "wollte", "starebbe", "они", "опять", "hadn", "damit", "aies", "aura", "ним", "avemmo", "кто", "те", "de", "кому", "indem", "soient", "avremmo", "habiendo", "because", "tenido", "étions", "eue", "delle", "dasselbe", "tuyo", "manchen", "stesse", "to", "разве", "которого", "degli", "stiano", "иногда", "hier", "faranno", "вниз", "куда", "does", "könnte", "такую", "estuvierais", "свой", "himself", "с", "тієї", "кого", "для", "hayáis", "свои", "sui", "une", "also", "vuestras", "nuestros", "than", "vostri", "could", "ayante", "ш", "seáis", "he", "stessero", "faceste", "shan", "unter", "hers", "ъ", "будет", "unseren", "может", "della", "тії", "могла", "моя", "хорошо", "habrían", "estéis", "furent", "up", "fûtes", "einen", "durch", "своего", "faccia", "тем", "per", "три", "оне", "viel", "them", "dort", "таким", "estarían", "contro", "ll", "ешь", "anderr", "zu", "hubiéramos", "while", "hubierais", "ont", "mis", "they", "aren", "д", "ведь", "nostri", "him", "наши", "самом", "которому", "nur", "моим", "fuiste", "зато", "farebbero", "einiges", "aber", "habríamos", "всё", "aux", "tuvo", "êtes", "ж", "собою", "stavo", "ту", "eurent", "herself", "doch", "такая", "starete", "над", "facciamo", "feci", "ero", "fareste", "о", "um", "которою", "fuéramos", "fummo", "моги", "kein", "avait", "мой", "hatten", "eues", "fuerais", "dai", "bist", "otra", "estarás", "à", "такому", "o", "из", "until", "самими", "è", "avevano", "alle", "nelle", "tus", "seríais", "seinem", "suis", "tenías", "как", "why", "keinem", "l", "avranno", "before", "am", "avesti", "avessimo", "had", "were", "if", "esa", "ничего", "avuti", "достаточно", "estada", "нашею", "estuviesen", "ayez", "todo", "auras", "sull", "можете", "habidos", "себе", "können", "и", "tiene", "leur", "einigem", "quante", "вот", "estuvo", "своею", "qué", "facevo", "aviez", "stia", "или", "cual", "между", "there", "saranno", "одному", "которым", "будешь", "после", "uno", "estuviera", "sto", "facendo", "конечно", "gewesen", "manche", "wo", "euch", "antes", "котором", "за", "тобой", "very", "которых", "quanto", "estés", "stettero", "sería", "которой", "avessero", "estoy", "ayons", "fussions", "tienes", "всего", "lo", "sie", "ем", "stavamo", "их", "теє", "вне", "avendo", "себя", "somos", "meinen", "своя", "eux", "dein", "нём", "mes", "algunos", "estas", "serás", "нами", "сами", "so", "jenen", "avete", "sareste", "vostre", "уже", "étés", "том", "qu", "hubisteis", "few", "faremo", "estuvieras", "ей", "самого", "unos", "keinen", "euer", "wasn", "eussent", "tuviste", "io", "tuvisteis", "sonst", "or", "fussiez", "farò", "yourself", "dich", "deiner", "отчего", "этой", "serons", "staranno", "siamo", "ese", "чего", "unseres", "такой", "ed", "теперь", "from", "nuestro", "bin", "themselves", "при", "vuestro", "avrai", "hat", "étiez", "cui", "нашем", "estará", "farai", "this", "estado", "at", "avevo", "während", "same", "sont", "serán", "out", "once", "quanti", "одни", "nell", "habéis", "было", "seine", "й", "нам", "tengas", "avevamo", "ses", "would", "starei", "auront", "самому", "ob", "sono", "тогда", "fue", "den", "facciano", "pero", "через", "тебя", "sta", "manches", "щ", "так", "вся", "whom", "avez", "donde", "fece", "esta", "vor", "hubieras", "tened", "and", "fuisteis", "notre", "anderm", "буду", "stetti", "stavi", "seiner", "serai", "in", "oder", "one", "solchem", "быть", "едят", "anderem", "всея", "that", "muchos", "nuestra", "i", "aie", "aient", "ч", "нашего", "habrás", "avrei", "tuvierais", "avec", "хоть", "такое", "poco", "weren", "estabais", "nuestras", "estarías", "jenes", "les", "facesti", "derer", "desde", "meinem", "fu", "ну", "mio", "derselbe", "aller", "ton", "you", "over", "tenida", "otro", "should", "hatte", "farà", "jedem", "erais", "таких", "мочь", "будут", "он", "shouldn", "above", "dei", "fuese", "такого", "tendremos", "quelli", "deinem", "staremmo", "otros", "habían", "faremmo", "ь", "такими", "anche", "ihrem", "keine", "having", "tendrá", "tuvimos", "снова", "haven", "hubieseis", "serait", "étées", "él", "je", "sois", "mia", "porque", "estás", "estuviese", "hubieran", "мною", "staresti", "coi", "нас", "него", "tendrán", "одного", "всеми", "могут", "più", "vosostros", "avons", "habe", "а", "seríamos", "estad", "meine", "sarebbe", "here", "did", "nei", "hayan", "fût", "tengamos", "perché", "комья", "estamos", "fossimo", "mon", "ain", "degl", "chi", "кроме", "li", "vi", "sintiendo", "estabas", "не", "mich", "этою", "demselben", "тому", "вас", "habríais", "negli", "werde", "serían", "eras", "wirst", "uns", "einigen", "warst", "своём", "jenem", "его", "tenidas", "этот", "однако", "deines", "dal", "wie", "ho", "fueses", "ел", "avuto", "них", "stiamo", "тут", "mí", "tuya", "ou", "saremo", "waren", "d", "ayant", "hubiésemos", "muy", "ins", "été", "which", "mein", "даже", "anders", "an", "abbiamo", "vous", "quien", "мной", "seraient", "наконец", "étais", "habré", "avaient", "tuvieseis", "hubo", "ve", "одна", "ihrer", "now", "той", "nichts", "от", "facevano", "cuando", "jeden", "tu", "всей", "facessimo", "ни", "aurai", "tengo", "своё", "weg", "fussent", "з", "eravamo", "dem", "my", "zum", "им", "haben", "одною", "stai", "очень", "wird", "если", "itself", "doing", "kann", "zur", "essendo", "han", "моего", "la", "unserem", "еще", "мог", "its", "questo", "чему", "habréis", "si", "étée", "stessimo", "могли", "ebbe", "esto", "само", "tanto", "моей", "надо", "teníamos", "suyos", "этому", "für", "facevi", "бы", "ф", "me", "ne", "stiate", "одним", "моих", "но", "estuviéramos", "fueran", "again", "staremo", "habrá", "esté", "elle", "suoi", "steste", "solcher", "tuviéramos", "di", "п", "ihm", "einig", "dalla", "both", "ihn", "своим", "facessero", "allo", "these", "abbiate", "hubieses", "tuvieras", "могло", "нашей", "starai", "avais", "each", "deine", "aurions", "theirs", "those", "ourselves", "mie", "che", "avesse", "había", "dov", "vuestros", "wenn", "loro", "agl", "два", "einmal"}

	// Получаем массив слов из строки
	sli := get_words_slice(text)

	for j := 0; j < len(sli); j++ {
		find := false
		for i := 0; i < len(array); i++ {

			if sli[j] == array[i] {
				find = true
				break
			}
		}

		if find == false {
			result = append(result, sli[j])
		}
	}

	return result
}

// Удаление дубликатов слов в массиве
func get_uniq_values(input []string) []string {
	// Если сделать это через map?
	result := make([]string, 0)
	words_count := make(map[string]int)

	for i := 0; i < len(input); i++ {
		words_count[input[i]] += 1
	}

	for i := 0; i < len(input); i++ {
		if words_count[input[i]] > 0 {
			result = append(result, input[i])
			words_count[input[i]] = 0
		}
	}

	return result
}

// Нахождение всех общих слов для параграфов и вопросов
func main() {
	var filename string
	//var filename_to_save string

	input := "test"

	if input == "train" {
		filename = "train_task1_latest.csv"
	} else if input == "test" {
		filename = "test_task1_latest.csv"
	}

	dat, err := ioutil.ReadFile(filename)
	check(err)

	csv_str := string(dat)

	r := csv.NewReader(strings.NewReader(csv_str))

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	// Обработка полученного текста
	for i0 := 100; i0 < 200; i0++ {
		text := records[i0][2]

		// Удаляем сноски
		text = removeFootnote(text)
		text = remove_symbols(text)
		text = strings.ToLower(text)

		text_slice := remove_stop_words(text)
		text_slice = get_uniq_values(text_slice)

		//result := make([]string, 0)

		for i := 0; i < len(text_slice); i++ {
			analyses := mystem.NewAnalyses(text_slice[i])
			defer analyses.Close()
			lemma := analyses.GetLemma(0)
			//result = append(result, lemma.Text())

			if lemma.Quality() == 1 {
				fmt.Println(lemma.Text() + " " + lemma.Form())
				fmt.Println(lemma.StemGram())
				fmt.Println(lemma.Quality())
			}
		}

		/*
			for i := 0; i < len(text_slice); i++ {
				analyses := mystem.NewAnalyses(text_slice[i])
				defer analyses.Close()
				lemma := analyses.GetLemma(0)
				result = append(result, lemma.Text())
				//fmt.Println(lemma.Text() + " " + lemma.Form())
				//fmt.Println(lemma.StemGram())
				// 136 - существительное
				// 137 - глагол
				// 128 - прилагательное
				// 138 - числительное-прилагательное второй
				// 133 - числительное два несколько

				// 129 - наречие
				// 135 - предлог
				// 139 - местоимение-прилагательное любой
				// 132 - междометие мм
				// 134 - частица исключительно
				// 130 - часть композита-сложного слова юго северо
				// 131 - союз нежели притом
				// 141 - местоимение-существительное
				// 140 - местоимение-наречие никак
				lemma_gram_slice := lemma.StemGram()
				if len(lemma_gram_slice) >= 1 {
					if lemma_gram_slice[0] != 129 && lemma_gram_slice[0] != 135 && lemma_gram_slice[0] != 136 && lemma_gram_slice[0] != 137 && lemma_gram_slice[0] != 128 && lemma_gram_slice[0] != 138 && lemma_gram_slice[0] != 133 && lemma_gram_slice[0] != 139 && lemma_gram_slice[0] != 132 && lemma_gram_slice[0] != 134 && lemma_gram_slice[0] != 130 && lemma_gram_slice[0] != 131 && lemma_gram_slice[0] != 141  && lemma_gram_slice[0] != 140 {
						fmt.Println(lemma.Text())
						fmt.Println(lemma.StemGram())
					}
				}
			}
		*/
	}
}

// https://tech.yandex.ru/mystem/doc/grammemes-values-docpage/
// https://github.com/dveselov/mystem/blob/master/constants.go
