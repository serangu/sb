import numpy
import xgboost
from sklearn import cross_validation
from sklearn.metrics import accuracy_score

# load data
dataset = numpy.loadtxt('pima-indians-diabetes.csv', delimiter=",")

# split data into X and y
X = dataset[:,0:8]
Y = dataset[:,8]

# split data into train and test sets
seed = 7
test_size = 0.33
X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, Y, test_size=test_size, random_state=seed)

# fit model no training data
eval_set = [(X_train, y_train), (X_test, y_test)]
model = xgboost.XGBClassifier(n_estimators=1000).fit(X_train, y_train, eval_set=eval_set, eval_metric="auc", early_stopping_rounds=30)
#model = xgboost.XGBClassifier().fit(X_train, y_train, eval_set=eval_set, eval_metric="auc", early_stopping_rounds=30)

print(model)

# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]

# evaluate predictions
accuracy = accuracy_score(y_test, predictions)
print("Accuracy: %.2f%%" % (accuracy * 100.0))