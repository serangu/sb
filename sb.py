import pandas as pd
from collections import Counter # Специализированный тип данных
import functools # Декораторы для функций
import tqdm # Быстрый прогресс-бар
import re # Регулярные выражения
import numpy as np
from sklearn import cross_validation
from sklearn.metrics import accuracy_score

import xgboost

# Импортируем пакеты с разными алгоритмами классификации
#from sklearn.cross_validation import train_test_split # Для разделения данных на 2 выборки
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression  # for Logistic Regression algorithm
from sklearn.neighbors import KNeighborsClassifier  # for K nearest neighbours
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn import svm  #for Support Vector Machine (SVM) Algorithm
from sklearn import metrics #for checking the model accuracy
from sklearn.tree import DecisionTreeClassifier #for using Decision Tree Algoithm
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

dftrain = pd.read_csv("train_task1_latest_my.csv")
dftest = pd.read_csv("test_task1_latest_my.csv")

columns = ['paragraph_lemmas_len', 'question_lemmas_len', 'intersection_lemmas_len', 
           'paragraph_lemmas_more_one_len', 'question_lemmas_more_one_len', 'intersection_lemmas_more_one_len', 
           #'idf_paragraph_lemmas_more_one', 'idf_question_lemmas_more_one', 'idf_intersection_lemmas_more_one',

           #'paragraph_bigram_lemmas_len', 'question_bigram_lemmas_len', 
           ###'intersection_bigram_lemmas_len', 
           'intersection_bigram_lemmas_proc',
           'intersection_lemmas_proc', 
           
           #'paragraph_trigram_lemmas_len', 'question_trigram_lemmas_len', 'intersection_trigram_lemmas_len', 'intersection_trigram_lemmas_proc',
           
           #'paragraph_lemmas_quality_0_proc', 'paragraph_lemmas_quality_1_proc', 'paragraph_lemmas_quality_8_proc',
           #'question_lemmas_quality_0_proc', 'question_lemmas_quality_1_proc', 'question_lemmas_quality_8_proc',
           
           #'paragraph_words_delta', 'question_words_delta',
           'idf_proc_question_lemmas_more_one',
           'question_words_count', 
           'question_uniq_words_count',
           'question_title_words_count', 
           'question_prep_symbols_count']
           #'similarity_value']

# split data into train and test sets
# split data into X and y
X = dftrain[columns]
Y = dftrain[['target']]

seed = 7
test_size = 0.33
X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, Y, test_size=test_size, random_state=seed)

# fit model no training data
eval_set = [(X_train, y_train), (X_test, y_test)]
model = xgboost.XGBClassifier(nthread=-1, seed=1, n_estimators=300).fit(X_train, y_train, eval_set=eval_set, eval_metric="auc", early_stopping_rounds=30)

print(model)

# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]

# evaluate predictions
accuracy = accuracy_score(y_test, predictions)
print("Accuracy: %.2f%%" % (accuracy * 100.0))

dftest['prediction'] = model.predict_proba(dftest[columns])[:, 1]

dftest.loc[dftest.prediction<0.05, ['prediction']] = 0.0

dftest[['paragraph_id', 'question_id', 'prediction']].to_csv("prediction_xgb.16.cross300.2.csv", index=False)


#for index, row in dftest.iterrows():
#    if row.question_uniq == 1: #and row.question_title_words_count <= 1:
#        if row.question_prep_symbols_count >= 3 or row.question_words_count - row.question_uniq_words_count >= 1:
#            dftest.loc[index, 'prediction'] = 0.0
   

'''
# Вторая модель
columns_2 = [ 'paragraph_lemmas_len', 'question_lemmas_len', 'intersection_lemmas_len', 
            'paragraph_bigram_lemmas_len', 'question_bigram_lemmas_len', 'intersection_bigram_lemmas_len', 
            'intersection_lemmas_proc', 'intersection_bigram_lemmas_proc',
            'paragraph_lemmas_more_one_len', 'question_lemmas_more_one_len', 'intersection_lemmas_more_one_len',
            'idf_proc_question_lemmas_more_one', 
            'question_words_count', 'question_uniq_words_count', 'question_title_words_count', 'question_prep_symbols_count']
            
model_2 = ExtraTreesClassifier(n_jobs=-1, random_state=1, n_estimators=1000, min_samples_split=8, min_samples_leaf=2).fit(dftrain[columns_2], dftrain['target'])
#dftest['prediction'] = model.predict_proba(dftest[columns])[:, 1]

#dftest.loc[dftest.prediction<0.05, ['prediction']] = 0.0

# Совместное предсказание
#dftest['prediction'] = model.predict_proba(dftest[columns])[:, 1]

one = model.predict(dftest[columns])
two = model_2.predict(dftest[columns_2])

result = []

i = 0
for value in one:
    if one[i] + two[i] >= 1.0:
        result.append(1.0)
    else:
        result.append(0.0)
    i = i + 1

dftest['prediction'] = result
dftest[['paragraph_id', 'question_id', 'prediction']].to_csv("prediction_ans_6.1.csv", index=False)

#model = xgboost.XGBClassifier(nthread=-1, seed=1, n_estimators=300).fit(dftrain[columns], dftrain['target'])
#dftest['prediction'] = model.predict_proba(dftest[columns])[:, 1]

#dftest.loc[dftest.prediction<0.05, ['prediction']] = 0.0

#for index, row in dftest.iterrows():
#    if row.question_uniq == 1: #and row.question_title_words_count <= 1:
#        if row.question_prep_symbols_count >= 3 or row.question_words_count - row.question_uniq_words_count >= 1:
#            dftest.loc[index, 'prediction'] = 0.0
        
        
        
        #else:
        #    if row.question_title_words_count <= 1:
        #        dftest.loc[index, 'prediction'] = 0.0
        #if row.prediction <= 0.7:
        #    dftest.loc[index, 'prediction'] = 0.0
    #else:
    #    if row.prediction < 0.70:
    #        df.loc[index, 'prediction'] = 0.0

# Попытка оставить только ответ с максимальным предсказанием для неуникальных ответов

# Оставляем 1 и 2 вопросы с максимальным прогнозом
top_predictions = {}
top2_predictions = {}

# Используем сложную метрику для оценки
# row.prediction+row.intersection_lemmas_proc+row.intersection_bigram_lemmas_proc*1.2
'''
'''
for index, row in dftest.iterrows():
    if row.question_uniq == 0 and row.prediction >= 0.5:
        temp = row.prediction+row.intersection_lemmas_proc+row.intersection_bigram_lemmas_proc*1.5
        
        if top_predictions.get(row.question_id) == None:
            top_predictions[row.question_id] = [row.paragraph_id, temp]
        else:
            if top_predictions[row.question_id][1] < temp:
                top_predictions[row.question_id] = [row.paragraph_id, temp]

# Сохраняем результат
for index, row in dftest.iterrows():
    if row.question_uniq == 0 and row.prediction >= 0.5:
        if top_predictions[row.question_id][0] != row.paragraph_id:
            dftest.loc[index, 'prediction'] = 0.0

'''
'''
for index, row in dftest.iterrows():
    if row.question_uniq == 0 and row.prediction >= 0.5:
        temp = row.prediction+row.intersection_lemmas_proc+row.intersection_bigram_lemmas_proc*1.5
        
        if top_predictions.get(row.question_id) == None:
            top_predictions[row.question_id] = [row.paragraph_id, temp]
            top2_predictions[row.question_id] = [row.paragraph_id, temp]
        else:
            if top_predictions[row.question_id][1] < temp:
                top2_predictions[row.question_id] = top_predictions[row.question_id]
                top_predictions[row.question_id] = [row.paragraph_id, temp]

# Сохраняем результат
for index, row in dftest.iterrows():
    if row.question_uniq == 0 and row.prediction >= 0.5:
        if top_predictions[row.question_id][0] != row.paragraph_id and top2_predictions[row.question_id][0] != row.paragraph_id:
            dftest.loc[index, 'prediction'] = 0.0

dftest[['paragraph_id', 'question_id', 'prediction']].to_csv("prediction_xgb.4.cross300.5+++top2.csv", index=False)
'''