import pandas as pd
from collections import Counter # Специализированный тип данных
import functools # Декораторы для функций
import tqdm # Быстрый прогресс-бар
import re # Регулярные выражения
import numpy as np
from sklearn import cross_validation
from sklearn.metrics import accuracy_score

import xgboost

# Импортируем пакеты с разными алгоритмами классификации
#from sklearn.cross_validation import train_test_split # Для разделения данных на 2 выборки
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression  # for Logistic Regression algorithm
from sklearn.neighbors import KNeighborsClassifier  # for K nearest neighbours
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn import svm  #for Support Vector Machine (SVM) Algorithm
from sklearn import metrics #for checking the model accuracy
from sklearn.tree import DecisionTreeClassifier #for using Decision Tree Algoithm
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

dftrain = pd.read_csv("train.csv")
dftest = pd.read_csv("test.csv")

columns = ['paragraph_lemmas_len', 'question_lemmas_len', 'intersection_lemmas_len', 
           'paragraph_bigram_lemmas_len', 'question_bigram_lemmas_len', 'intersection_bigram_lemmas_len',
           'intersection_lemmas_proc', 'intersection_bigram_lemmas_proc',
           'len_paragraph_lemmas_more_one', 'len_question_lemmas_more_one', 'len_intersection_lemmas_more_one', 
           'idf_paragraph_lemmas_more_one', 'idf_question_lemmas_more_one', 'idf_intersection_lemmas_more_one']


# split data into train and test sets
# split data into X and y
X = dftrain[columns]
Y = dftrain[['target']]

seed = 7
test_size = 0.33
X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, Y, test_size=test_size, random_state=seed)

# fit model no training data
eval_set = [(X_train, y_train), (X_test, y_test)]
model = xgboost.XGBClassifier(nthread=-1, seed=1, n_estimators=1000).fit(X_train, y_train, eval_set=eval_set, eval_metric="auc", early_stopping_rounds=30)

print(model)

# make predictions for test data
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]

# evaluate predictions
accuracy = accuracy_score(y_test, predictions)
print("Accuracy: %.2f%%" % (accuracy * 100.0))

'''
dftest['prediction'] = model.predict_proba(dftest[columns])[:, 1]

dftest.loc[dftest.prediction<0.05, ['prediction']] = 0.0

for index, row in dftest.iterrows():
    if row.question_uniq == 1:
        if row.question_prep_symbols_count >= 3 or row.question_words_count - row.question_uniq_words_count >= 1:
            dftest.loc[index, 'prediction'] = 0.0

dftest[['paragraph_id', 'question_id', 'prediction']].to_csv("prediction_xgb.3.15cross+.csv", index=False)
'''